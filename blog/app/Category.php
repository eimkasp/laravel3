<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class Category extends Model
{
	//
	protected $table = "categories";

	public $timestamps = false;

	public static function topLevelCategories() {
		return Category::where('category_id', null)->with('subCategories');
	}

	public static function cachedCategories() {
		return Cache::remember('top_level_categories', 60, function() {
			return Category::where('category_id', null)->get();
		});
	}

	public function subCategories() {
		return $this->hasMany('App\Category', 'category_id', 'id');
	}

	public function news() {
		return $this->belongsToMany('App\NewsItem', 'news_categories', 'category_id', 'news_item_id');
	}
}
