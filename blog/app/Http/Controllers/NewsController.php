<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\NewsRequest;
use App\NewsItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class NewsController extends Controller {

	public function __construct() {
		//$this->middleware('admin')->except(['index', 'show']);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//
		$page = $request->input( 'page', '1' );
		$news = Cache::remember( 'news_index_page_' . $page, 60, function () {
			return NewsItem::orderBy( 'created_at', 'DESC' )->with( 'user' )->paginate( 50 );
		});



		return view( 'news.index', [ 'news' => $news] );

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//

		$categories = Category::topLevelCategories()->get();

		return view( 'news.create', [
			"categories" => $categories
		] );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( NewsRequest $request ) {
		//
		$newsItem          = new NewsItem();
		$newsItem->title   = $request->input( 'title' );
		$newsItem->content = $request->input( 'content' );
		$newsItem->user_id = Auth::user()->id;
//		$newsItem->language = App::getLocale();

		if ( $request->has( 'image' ) ) {
			$imagePath       = $request->file( 'image' )->store( 'news-images' );
			$newsItem->image = $imagePath;
		}

		$newsItem->save();

		$newCategories = $request->input( 'categories' );
		$newsItem->categories()->sync( $newCategories );


		return redirect()->route( 'news.index' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//

		$item = NewsItem::with( [ 'topLevelComments', 'user', 'categories' ] )->find( $id );


		return view( 'news.show', [ 'item' => $item ] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//

		$newsItem   = NewsItem::find( $id );
		$categories = Category::all();

		return view( 'news.edit', [ 'newsItem' => $newsItem, 'categories' => $categories ] );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
		$newsItem          = NewsItem::find( $id );
		$newsItem->title   = $request->input( 'title' );
		$newsItem->content = $request->input( 'content' );


		if ( $request->has( 'image' ) ) {
			$imagePath       = $request->file( 'image' )->store( 'news-images' );
			$newsItem->image = $imagePath;
		}

		$newCategories = $request->input( 'categories' );
		$newsItem->categories()->sync( $newCategories );

		$newsItem->save();

		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
