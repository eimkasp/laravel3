<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

		list($subdomain,$host) = explode('.', $_SERVER["SERVER_NAME"]);

		App::setLocale($subdomain);
        return $next($request);
    }
}
