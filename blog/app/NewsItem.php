<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsItem extends Model {
	//
	protected $table = "news";

	public function user() {
		return $this->hasOne( 'App\User', 'id', 'user_id' );
	}


	public function categories() {
		return $this->belongsToMany( 'App\Category', 'news_categories', 'news_item_id', 'category_id' );
	}

	public function comments() {
		return $this->hasMany( 'App\Comment', 'news_id', 'id' );
	}

	public function topLevelComments() {
		return $this->hasMany( 'App\Comment', 'news_id', 'id' )
					->with( ['user', 'replies'])
					->where( 'comment_id', '=', null );
	}

	public static function getSubCategoryNews($categoryID) {

		$category = Category::find($categoryID);
		$subCategoriesIds = [$categoryID];

		foreach ($category->subCategories as $subCategory) {
			$subCategoriesIds[] = $subCategory->id;
		}


		return 	NewsItem::whereHas('categories', function ($query) use ($subCategoriesIds) {
			$query->whereIn('id', $subCategoriesIds );
		})->orderBy('created_at', "DESC");
	}
}