<?php

namespace App\Providers;

use App\Category;
use Illuminate\Support\ServiceProvider;

class CategoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
	{
		$categoriesProvider = Category::cachedCategories();


		view()->composer('*', function ($view) use ($categoriesProvider){
			$view->with('categoriesProvider', $categoriesProvider);
		});
    }
}
