<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\NewsItem;
use Faker\Generator as Faker;

$factory->define(NewsItem::class, function (Faker $faker) {
    return [
        //
		'title' => $faker->realText("191"),
		'image' => $faker->imageUrl(640, 480),
		'content' => $faker->text('64000'),
		'user_id' => 1
    ];
});
