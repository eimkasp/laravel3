<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesAndForeignKeysToNewsAndCategories extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'news_categories', function ( Blueprint $table ) {
			$table->index( 'news_item_id' );
			$table->index( 'category_id' );

			$table->foreign( 'news_item_id' )
				  ->references( 'id' )
				  ->on( 'news' )
				  ->onDelete( 'cascade' );


			$table->foreign( 'category_id' )
				  ->references( 'id' )
				  ->on( 'categories' )
				  ->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'news_categories', function ( Blueprint $table ) {
			//
			$table->dropForeign(['news_item_id']);
			$table->dropForeign(['category_id']);

			$table->dropIndex(['news_item_id']);
			$table->dropIndex(['category_id']);
		} );
	}
}
