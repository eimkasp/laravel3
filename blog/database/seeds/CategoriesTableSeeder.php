<?php

use Illuminate\Database\Seeder;

use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		$cat1 = new Category();
		$cat1->title = "Naujienos";
		$cat1->save();

		$cat2 = new Category();
		$cat2->title = "Sportas";
		$cat2->save();

		$cat3 = new Category();
		$cat3->title = "Sveikata";
		$cat3->save();
    }
}
