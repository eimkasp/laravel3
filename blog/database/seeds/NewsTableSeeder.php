<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		//

		$news = factory(App\NewsItem::class, 1000)->create()->each(function($newsItem) {
			$categoriesCount = rand(1, 3);
			$categoriesToAdd = [];
			for($i = 0; $i < $categoriesCount; $i++) {
				$categoriesToAdd[]  = \App\Category::inRandomOrder()->first()->id;
			}

			$newsItem->categories()->sync($categoriesToAdd);

		});


	}
}
