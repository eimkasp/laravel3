<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		$user1 = new \App\User();
		$user1->name = "Administratorius";
		$user1->email = "test@admin.lt";
		$user1->password =  bcrypt('secret');
		$user1->role = "admin";

		$user1->save();


		$user2 = new \App\User();
		$user2->name = "Paprastas vartotojas";
		$user2->email = "test@user.lt";
		$user2->password =  bcrypt('secret');
		$user2->role = "user";

		$user2->save();
    }
}
