CKEDITOR.replace('content', {
    uiColor: '#CCEAEE',
    removePlugins: 'wsc',
    // Configure SCAYT to load on editor startup.
    scayt_autoStartup: true,
    // Limit the number of suggestions available in the context menu.
    scayt_maxSuggestions: 3
});