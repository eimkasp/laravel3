@extends('layouts.app')

@section('content')
	<div class="container">
		<h1>{{ __('Categories list') }} </h1>

		<table class="table">
			@foreach($categories as $category)
				<tr>
					<td>
						{{ $category->id }}
					</td>

					<td>
						<a href="{{ route('categories.show', [$category->id]) }}">
							{{ $category->title }}
						</a>
					</td>
					<td>
						News count {{ $category->news()->count() }}
					</td>
				</tr>
			@endforeach
		</table>
	</div>
@endsection