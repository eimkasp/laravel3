@extends('layouts.app')

@section('content')
    <div class="container">
        <h1> Category: {{ $category->title }} </h1>
        <div class="row">
            <div class="col-md-2">
                <h3>Sub kategorijos</h3>

                <ul>
                    @foreach($category->subCategories as $subCategory)
                        <li>
                            <a href="{{ route('categories.show', [$subCategory->id]) }}">
                                {{ $subCategory->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-10">
                <div class="row">
                    @foreach($categoryNews as $item)
                        @include('components.news-item')
                    @endforeach
                </div>


                {{ $categoryNews->links()}}
            </div>


        </div>
    </div>
@endsection