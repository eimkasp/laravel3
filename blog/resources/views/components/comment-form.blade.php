<form method="post" action="{{ route('comments.store') }}">
    <h3>Add new comment</h3>
    @csrf

    <textarea name="content" placeholder="Enter your comment" class="form-control"></textarea>

    <input type="hidden" name="news_id" value="{{ $item->id }}">
    <input type="submit" class="btn btn-success mt-3">
</form>