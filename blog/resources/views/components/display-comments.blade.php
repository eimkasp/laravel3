<h3 class="mt-3">Comments ({{ $item->comments()->count() }})</h3>

@foreach($item->topLevelComments as $comment)
    <div class="comment card mt-3">
        <div class="card-body">
            <h5 class="comment-author card-title">
                {{ $comment->user->name }}
            </h5>

            <div class="comment-content">
                {{ $comment->content }}
            </div>

            {{-- Atsakymu spausdinimas --}}
            @foreach($comment->replies as $reply)
                <div class="card">
                    <div class="card-body">
                        <h6>{{ $reply->user->name }}</h6>
                        <div class="reply-content">
                            {{ $reply->content }}
                        </div>
                    </div>
                </div>
            @endforeach


            {{-- Atsakymo rasymo forma --}}

            <form method="post" action="{{ route('comments.store') }}" class="mt-3">
                @csrf
                <textarea name="content" class="form-control" placeholder="Reply to this comment"></textarea>

                <input type="hidden" name="news_id" value="{{ $item->id }}">

                <input type="hidden" name="comment_id" value="{{ $comment->id }}">

                <input type="submit" value="Comment" class="btn btn-success mt-3">

            </form>
        </div>
    </div>

@endforeach