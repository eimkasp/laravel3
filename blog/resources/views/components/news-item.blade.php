<article class="col-md-6">
    <h3>
        <a href="{{ route('news.show', [$item->id]) }}">
            {{ $item->title }}
        </a>

        <small>
            <a href="{{ route('news.edit', [$item->id])}}">Edit </a>
        </small>
    </h3>

    <small>Author: {{ $item->user->name }}</small>


    @isset($item->image)

        {{-- Tikriname ar nuotrauka yra is storage ar is nuotolinio source --}}
        @if(strpos($item->image, "https://") !== false || strpos($item->image, "http://") !== false)
            <img src="{{ $item->image }} " style="max-width: 100%">
        @else
            <img src="{{ Storage::url($item->image) }}" style="max-width: 100%">
        @endif
    @endisset

    <div class="execerpt">
        {!!  substr(strip_tags($item->content),0, 100) !!}...
    </div>
</article>