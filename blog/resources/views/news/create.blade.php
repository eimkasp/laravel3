@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="post" action="{{ route('news.store') }}" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <h1>Create news item</h1>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @csrf

                    @method('post')

                    <div class="form-group">
                        <input type="text" name="title" placeholder="Title" required class="form-control"
                               value="{{ old('title') }}">
                    </div>

                    <div class="form-group">
                        <textarea name="content">{{ old('content') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" name="image" class="form-control">
                    </div>

                    <input type="submit" value="Save" class="btn btn-success">


                </div>
                <div class="col-md-6">

                    @foreach($categories as $category)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="categories[]"
                                   value="{{ $category->id }}">
                            <label class="form-check-label">{{ $category->title }}</label>
                        </div>
                        <div class="sub-categories pl-3">
                            @foreach($category->subCategories as $subCategory)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="categories[]"
                                           value="{{ $subCategory->id }}">
                                    <label class="form-check-label"> {{ $subCategory->title }}</label>
                                </div>

                            @endforeach
                        </div>
                    @endforeach


                </div>
            </div>
        </form>

    </div>
@endsection