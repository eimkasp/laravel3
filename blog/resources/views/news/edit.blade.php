@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="post" action="{{ route('news.update', [$newsItem->id]) }}" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <h1>Edit news item: {{ $newsItem->title }}</h1>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @csrf

                    @method('put')

                    <div class="form-group">
                        <input type="text" name="title" placeholder="Title" required class="form-control"
                               value="{{ $newsItem->title }}">
                    </div>

                    <div class="form-group">
                        <textarea name="content" class="form-control"
                                  placeholder="Enter content here">{{ $newsItem->content }}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" name="image" class="form-control">
                    </div>

                    <input type="submit" value="Save" class="btn btn-success">


                </div>
                <div class="col-md-6">

                    @foreach($categories as $category)
                        <div class="form-check">


                            <input class="form-check-input" type="checkbox" name="categories[]"
                            @foreach($newsItem->categories as $cat)
                                @if($cat->id == $category->id)
                                    checked
                                    @break
                                @endif
                            @endforeach
                                   value="{{ $category->id }}">
                            <label class="form-check-label">{{ $category->title }}</label>
                        </div>
                    @endforeach


                </div>
            </div>
        </form>

    </div>
@endsection