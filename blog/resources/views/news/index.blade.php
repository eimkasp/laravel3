@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($news as $item)
                @include('components.news-item')
            @endforeach

            <div>
                {{ $news->links() }}
            </div>
        </div>
    </div>
@endsection