@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>{{ $item->title }}</h1>
                <p>
                    {!!  $item->content !!}
                </p>

                <div class="comments">
                    @include('components.comment-form')
                </div>

                <div class="display-comments">
                    @include('components.display-comments')
                </div>
            </div>

            <div class="col-md-4">
                <h3>
                    Kategorijos
                </h3>
                <ul>
                    @foreach($item->categories as $category)
                        <li>{{ $category->title }}</li>
                    @endforeach
                </ul>

            </div>
        </div>

    </div>
@endsection